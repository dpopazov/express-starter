Setup

Create and fill out environment variables in .env file!
Example in .env.example.

Running in development mode

`docker-compose up`

For running in production mode you need to change Dockerfile.local to Dockerfile.prod
in docker-compose.yml

Then also run!

`docker-compose up`