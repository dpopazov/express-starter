const express = require('express');
const bodyParser = require('body-parser');
const expressJwt = require('express-jwt');
const { CronJob } = require('cron');
const fs = require("fs");
const cors = require('cors');


const app = new express();
const port = process.env.SERVER_PORT;

const dayStart = new Date().setUTCHours(0,0,0,0);
let logFile = fs.createWriteStream(__dirname + '/logs/' + `${dayStart}.log`);

const logging = require('./middlewares/logging');

const user = require('./user/router');
const auth = require('./auth/router');
const payment = require('./payment/router');

app.use(cors());
app.use(bodyParser.json());
app.use((...args) => logging(logFile, ...args));

const openRoutes = [
  '/auth/login',
  '/auth/sign-up',
  '/payment/order'
];
app.use(expressJwt({ secret: process.env.JWT_SECRET_KEY, algorithms: ['HS256'] }).unless({ path: openRoutes }));

app.use('/user', user);
app.use('/auth', auth);
app.use('/payment', payment);



const job = new CronJob('00 00 00 * * *', () => {
  logFile = fs.createWriteStream(__dirname + '/logs/' + `${Date.now()}.log`);
});

job.start();


app.listen(port, () => {
  console.log(`Server started on port ${port}`);
});
