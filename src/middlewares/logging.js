const util = require("util");

const logging = (logFile, req, res, next) => {
  logFile.write(util.format('=========== Request time ===> ' + new Date().toISOString() + ' ===============') + '\n');
  logFile.write(util.format('=========== Request url ===> ' + req.originalUrl + ' ===============') + '\n');
  logFile.write(util.format('=========== Request Method ===> ' + req.method + ' ===============') + '\n');

  next();
};

module.exports = logging;