const express = require('express');
const { handleUserLogin, handleUserSignUp} = require("./controller");


const router = express.Router();


router.post('/login', handleUserLogin);
router.post('/sign-up', handleUserSignUp);


module.exports = router;