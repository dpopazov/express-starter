const {
  handleUserLogin,
  handleUserSignUp
} = require('./controller');

jest.mock('sequelize');
jest.mock('bcrypt', () => ({
  compare: jest.fn().mockReturnValue(true)
}));

const req = {
  body: {
    email: 'test@mail.com',
    password: 'test_password'
  }
};
const res = {
  send: jest.fn(),
  end: jest.fn()
};

const user = {
  id: 1,
  email: 'test@mail.com',
  password: 'test_password'
}

jest.mock('../user/service', () => ({
  getUserByEmail: jest.fn().mockReturnValue(user),
  createUser: jest.fn().mockReturnValue(user)
}));
const { getUserByEmail, createUser } = require("../user/service");


describe('Auth controller', () => {
  describe('handleUserLogin', () => {
    test('should call getUserByEmail user service method', async () => {
      await handleUserLogin(req, res);

      expect(getUserByEmail).toHaveBeenCalledWith(req.body.email);
    });

    test('should call res methods', async () => {
      await handleUserLogin(req, res);

      expect(res.send).toHaveBeenCalled();
      expect(res.end).toHaveBeenCalled();
    });
  });

  describe('handleUserSignUp', () => {
    test('should call createUser user service method', async () => {
      await handleUserSignUp(req, res);

      expect(createUser).toHaveBeenCalledWith(req.body);
    });

    test('should call res methods', async () => {
      await handleUserSignUp(req, res);

      expect(res.send).toHaveBeenCalledWith(user);
      expect(res.end).toHaveBeenCalled();
    });
  });
});
