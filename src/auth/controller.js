const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const config = require('../config/common');

const { getUserByEmail, createUser } = require("../user/service");

const handleUserLogin = async (req, res) => {
  const { password, id, email } = await getUserByEmail(req.body.email);

  const match = await bcrypt.compare(req.body.password, password);

  if (match) {
    const token = jwt.sign({ sub: id, email }, config.jwtSecretKey, { expiresIn: '24h' });

    res.send({ accessToken: token });
  }

  res.end();
};

const handleUserSignUp = async (req, res) => {
  const createdUser = await createUser(req.body);

  res.send(createdUser);
  res.end();
};

module.exports = {
  handleUserLogin,
  handleUserSignUp
};
