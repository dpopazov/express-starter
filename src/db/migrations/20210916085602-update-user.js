'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('users', 'customer_id', Sequelize.STRING);
  },


down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn('users', 'customer_id', Sequelize.STRING);
  }
};
