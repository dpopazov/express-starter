const variables = {
  development: {
    jwtSecretKey: process.env.JWT_SECRET_KEY,
    stripeSecretKey: process.env.STRIPE_SECRET_KEY,
  },
  production: {
    jwtSecretKey: process.env.JWT_SECRET_KEY,
    stripeSecretKey: process.env.STRIPE_SECRET_KEY,
  },
  test: {
    jwtSecretKey: 'test_key'
  }
}

module.exports = variables[process.env.NODE_ENV];
