const db = require('../db/models');

const getAllUsers = async () => {
  try {
    return await db.user.findAll();
  } catch (err) {
    console.error(err.message);

    return err.message;
  }
};

const createUser = async user => {
  try {
    const dbUser =  await db.user.create(user);

    const { password, ...rest } = dbUser.dataValues;

    return rest;
  } catch (err) {
    console.error(err.message);

    return err.message;
  }
};

const getUserByEmail = async email => {
  try {
    const { dataValues } = await db.user.findOne({ where: { email } });

    return dataValues;
  } catch (err) {
    console.error(err.message);

    return err.message;
  }
};

const getUserById = async id => {
  try {
    const { dataValues } = await db.user.findOne({ where: { id } });

    return dataValues;
  } catch (err) {
    console.error(err.message);

    return err.message;
  }
};

const updateUser = async user => {
  try {
    const [isUpdated] =  await db.user.update(user, { where: { id: user.id } });


    if (isUpdated) {
      const { dataValues } = await db.user.findOne({ where: { id: user.id } });

      return dataValues;
    }


    return null;
  } catch (err) {
    console.error(err.message);

    return err.message;
  }
}

module.exports = {
  getAllUsers,
  createUser,
  updateUser,
  getUserByEmail,
  getUserById
};
