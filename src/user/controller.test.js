const { handleUsersGet } = require("./controller");

const users = [
  { id: 1, name: 'First user' },
  { id: 2, name: 'Second user' },
];


jest.mock('sequelize');
jest.mock('./service', () => ({
  getAllUsers: jest.fn().mockReturnValue(users)
}));

describe('User controller', () => {
  describe('handleUsersGet', () => {

    test('should call res methods', async () => {
      const req = {};
      const res = {
        send: jest.fn(),
        end: jest.fn()
      };

      await handleUsersGet(req, res);

      expect(res.send).toHaveBeenCalledWith(users);
      expect(res.end).toHaveBeenCalled();
    });
  });
});
