const {
  getAllUsers,
  createUser,
  getUserByEmail
} = require('./service');

const createdUser = { password: 'test_password', email: 'test@mail.com' };

jest.mock('sequelize');
jest.mock('../db/models', () => ({
  user: {
    findAll: jest.fn(),
    findOne: jest.fn().mockReturnValue({}),
    create: jest.fn().mockReturnValue({ dataValues: createdUser }),
  }
}));
const db = require('../db/models');


describe('User service', () => {
  describe('getAllUsers', () => {
    test('should call findAll method', async () => {

      await getAllUsers();
      expect(db.user.findAll).toHaveBeenCalled();
    });
  });

  describe('createUser', () => {
    test('should call create method', async () => {

      const user = { email: 'test@mail.com' }

      await createUser(user);
      expect(db.user.create).toHaveBeenCalledWith(user);
    });
  });

  describe('getUserByEmail', () => {
    test('should call findOne method', async () => {

      const email = 'test@mail.com';

      await getUserByEmail(email);
      expect(db.user.findOne).toHaveBeenCalledWith({ where: { email } });
    });
  });
});
