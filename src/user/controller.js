const { getAllUsers, getUserById, updateUser } = require('./service');


const handleUsersGet = async (req, res) => {
  const users = await getAllUsers();
  res.send(users);
  res.end();
};

const handleUserAvatar = async (req, res) => {

  let user = await getUserById(req.user.sub);

  if (user) {
    user = await updateUser({ ...user, avatar: req.file.location });
  }

  res.send(user);
  res.end();
};

module.exports = {
  handleUsersGet,
  handleUserAvatar
};
