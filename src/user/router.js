const express = require('express');
const multer = require('multer');
const multerS3 = require('multer-s3');
const { S3 } = require('aws-sdk');

const { handleUsersGet, handleUserAvatar } = require("./controller");

const s3 = new S3();
const router = express.Router();

const upload = multer({
  storage: multerS3({
    s3,
    bucket: 'node-js-lesson',
    metadata: function (req, file, cb) {
      cb(null, { fieldName: file.fieldname });
    },
    key: function (req, file, cb) {
      cb(null, `avatars/${Date.now().toString()}-${file.originalname}`)
    }
  })
});


router.get('/', handleUsersGet);
router.post('/avatar', upload.single('file'), handleUserAvatar);


module.exports = router;
