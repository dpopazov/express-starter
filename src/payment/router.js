const express = require('express');
const { handleOrder } = require("./controller");


const router = express.Router();


router.post('/order', handleOrder);


module.exports = router;
