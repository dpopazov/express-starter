const config = require('../config/common');

const stripe = (require('stripe')(config.stripeSecretKey));


const { getPaymentMethod, createPayment } = require('./service');
const { getUserById, updateUser } = require("../user/service");


const handleOrder = async (req, res) => {
  const {
    order_id,
    user_id,
    token,
    amount
  } = req.body;

  let user = await getUserById(user_id);

  if (user && !user.customer_id) {
    const customer = await stripe.customers.create({
      email: user.email,
      name: `${user.first_name} ${user.last_name}`
    });

    user = await updateUser({ ...user, customer_id: customer.id });
  }

  let payment = await getPaymentMethod(user_id);

  if (!payment) {
    const source = await stripe.customers.createSource(
      user.customer_id,
      { source: token }
    );

    payment = await createPayment({
      stripe_id: source.id,
      user_id: user.id,
      fingerprint: source.fingerprint
    });
  }

  const charge = await stripe.charges.create({
    source: payment.stripe_id,
    amount,
    currency: 'usd',
    description: `user_id: ${user_id} paid ${amount} for order - ${order_id}`,
    customer: user.customer_id
  });

  res.send(charge);
  res.end();
};

module.exports = {
  handleOrder,
};
