const db = require('../db/models');

const getPaymentMethod = async userId => {
  try {
    const payment = await db.payment.findOne({ where: { user_id: userId } });


    return payment ? payment.dataValues : null;
  } catch (err) {
    console.error(err.message);

    return err.message;
  }
};

const createPayment = async payment => {
  try {
    const { dataValues } =  await db.payment.create(payment);

    return dataValues;
  } catch (err) {
    console.error(err.message);

    return err.message;
  }
};



module.exports = {
  getPaymentMethod,
  createPayment
};
